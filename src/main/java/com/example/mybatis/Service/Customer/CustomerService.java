package com.example.mybatis.Service.Customer;

import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Model.request.CustomerRequest;


import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer id);

    Customer addNewCustomer(CustomerRequest customerRequest);

    Customer updateCustomerById(Integer id, CustomerRequest customerRequest);

    Customer deleteCustomerById(Integer id);
}
