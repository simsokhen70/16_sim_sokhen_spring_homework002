package com.example.mybatis.Service.Customer;


import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Model.request.CustomerRequest;
import com.example.mybatis.Repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImplement implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImplement(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.addNewCustomer(customerRequest);
    }

    @Override
    public Customer updateCustomerById(Integer id, CustomerRequest customerRequest) {
        return customerRepository.updateCustomerById(id,customerRequest);
    }

    @Override
    public Customer deleteCustomerById(Integer id) {
        return customerRepository.deleteCustomerById(id);
    }

}
