package com.example.mybatis.Service.Invoice;

import com.example.mybatis.Model.entity.Invoice;
import com.example.mybatis.Model.request.InvoiceRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InvoiceService {


    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer id);

    Integer addNewInvoice(InvoiceRequest invoiceRequest);
}
