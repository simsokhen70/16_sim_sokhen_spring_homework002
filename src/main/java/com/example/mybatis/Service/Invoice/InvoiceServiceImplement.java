package com.example.mybatis.Service.Invoice;


import com.example.mybatis.Model.entity.Invoice;
import com.example.mybatis.Model.request.InvoiceRequest;
import com.example.mybatis.Repository.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImplement implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImplement(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        return invoiceRepository.addNewInvoice(invoiceRequest).getInvoiceId();
    }


}
