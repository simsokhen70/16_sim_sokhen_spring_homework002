package com.example.mybatis.Service.Product;

import com.example.mybatis.Model.entity.Product;
import com.example.mybatis.Model.request.ProductRequest;
import com.example.mybatis.Repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplement implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImplement(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product addNewProduct(ProductRequest productRequest) {
        return productRepository.addNewProduct(productRequest);
    }

    @Override
    public Product updateProductById(Integer id, ProductRequest productRequest) {
        return productRepository.updateProductById(id,productRequest);
    }

    @Override
    public Product deleteProductById(Integer id) {
        return productRepository.deleteProductById(id);
    }


}
