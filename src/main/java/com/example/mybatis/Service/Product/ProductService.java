package com.example.mybatis.Service.Product;

import com.example.mybatis.Model.entity.Product;
import com.example.mybatis.Model.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer id);

    Product addNewProduct(ProductRequest productRequest);

    Product updateProductById(Integer id, ProductRequest productRequest);


    Product deleteProductById(Integer id);
}
