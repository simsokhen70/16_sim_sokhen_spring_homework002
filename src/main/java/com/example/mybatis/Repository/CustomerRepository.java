package com.example.mybatis.Repository;

import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Model.entity.Invoice;
import com.example.mybatis.Model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    // Get All Customer
    @Select("""
            SELECT * 
            FROM customer_tb
            """)
    @Results(id = "customerMap", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_Phone")
    })
    List<Customer> getAllCustomer();


    // Get a customer by id
    @Select("""
            SELECT * 
            FROM customer_tb 
            WHERE customer_id = #{id};
            """)
    @ResultMap("customerMap")
    Customer getCustomerById(Integer id);

    // Insert new customer
    @Select("""
            INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
            VALUES (#{cus.customerName}, #{cus.customerAddress}, #{cus.customerPhone})
            RETURNING *
            """)
    @ResultMap("customerMap")
    Customer addNewCustomer(@Param("cus") CustomerRequest customerRequest);


    // Update customer
    @Select("""
            UPDATE customer_tb
            SET customer_name = #{customer.customerName}, customer_address = #{customer.customerAddress}, customer_phone = #{customer.customerPhone}
            WHERE customer_id = #{id}
            """)
    @ResultMap("customerMap")
    Customer updateCustomerById(Integer id,@Param("customer") CustomerRequest customerRequest);


    // Delete customer
    @Select("""
            DELETE FROM customer_tb
            WHERE customer_id = #{id}
            """)
    @ResultMap("customerMap")
    Customer deleteCustomerById(Integer id);


}
