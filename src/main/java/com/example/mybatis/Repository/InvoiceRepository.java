package com.example.mybatis.Repository;

import com.example.mybatis.Model.entity.Invoice;
import com.example.mybatis.Model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    // Get All Invoice
    @Select("""
            SELECT * 
            FROM invoice_tb
            """)
    @Result(property = "customerId", column = "customer_id",
            one = @One(select = "com.example.mybatis.Repository.CustomerRepository.getCustomerById")
    )

    @Result(property = "invoiceId", column = "invoice_id")
    @Result(property = "invoiceDate", column = "invoice_date")
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.example.mybatis.Repository.ProductRepository.getAllByProductId")
    )

    List<Invoice> getAllInvoice();


    // Get Invoice by id
    @Select("""
            SELECT * 
            FROM invoice_tb
            WHERE invoice_id = #{id}
            """)
    @Result(property = "customerId", column = "customer_id",
            one = @One(select = "com.example.mybatis.Repository.CustomerRepository.getCustomerById")
    )

    @Result(property = "invoiceId", column = "invoice_id")
    @Result(property = "invoiceDate", column = "invoice_date")
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.example.mybatis.Repository.ProductRepository.getAllByProductId")
    )

    Invoice getInvoiceById(Integer id);

//    // Add new invoice

@Insert("""
        INSERT INTO invoice_tb(invoice_date, customer_id)
        VALUES (#{inv.invoiceDate}, #{inv.customerId})
        RETURNING invoice_id, invoice_date, customer_id
        """)
@Results({
        @Result(property = "customerId", column = "customer_id",
                one = @One(select = "com.example.mybatis.Repository.CustomerRepository.getCustomerById")),
        @Result(property = "invoiceId", column = "invoice_id"),
        @Result(property = "invoiceDate", column = "invoice_date"),
        @Result(property = "products", column = "invoice_id",
                many = @Many(select = "com.example.mybatis.Repository.ProductRepository.getAllByProductId"))
})
Invoice addNewInvoice(@Param("inv") InvoiceRequest invoiceRequest);
}
