package com.example.mybatis.Repository;

import com.example.mybatis.Model.entity.Product;
import com.example.mybatis.Model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface ProductRepository {
    @Select("""
            SELECT i.product_id, i.product_name, i.product_price
            FROM product_tb i INNER JOIN invoice_detail_tb invd
            ON i.product_id = invd.product_id
            WHERE invoice_id = #{id};
            """)
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_price")
    List<Product> getAllByProductId(@Param("id") Integer invoiceId);



    // Get all products
    @Select("""
            SELECT * 
            FROM product_tb
            """)
    @Results(id = "productMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price"),
    })
    List<Product> getAllProduct();

    // Get product by id
    @Select("""
            SELECT * 
            FROM product_tb 
            WHERE product_id = #{id};
            """)
    @ResultMap("productMap")
    Product getProductById(Integer id);

    // Insert new product
    @Select("""
            INSERT INTO product_tb (product_name, product_price)
            VALUES (#{pro.productName}, #{pro.productPrice})
            RETURNING *
            """)
    @ResultMap("productMap")
    Product addNewProduct(@Param("pro") ProductRequest productRequest);

    // Update product
    @Select("""
            UPDATE product_tb
            SET product_name = #{product.productName}, product_price = #{product.productPrice}
            WHERE product_id = #{id}
            """)
    @ResultMap("productMap")
    Product updateProductById(Integer id,@Param("product") ProductRequest productRequest);

    // Delete Product
    @Select("""
            DELETE FROM product_tb
            WHERE product_id = #{id}
            """)
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_price")
    Product deleteProductById(Integer id);
}
