package com.example.mybatis.Model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private Integer invoiceId;
    private String invoiceDate;
    private Customer customerId;

    private List<Product> products;
}
