package com.example.mybatis.Model.request;

import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {
    private String invoiceDate;
    private Customer customerId;

    private List<Product> products;
}
