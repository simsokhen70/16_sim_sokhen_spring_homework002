package com.example.mybatis.Controller;

import com.example.mybatis.Model.request.CustomerRequest;
import com.example.mybatis.Model.response.ApiResponse;
import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Service.Customer.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    // Get all customers
    @GetMapping("/get-all-customer")
    public ApiResponse getAllCustomer() {
        // Retrieve customer information from database or other data source
        List<Customer> customer = customerService.getAllCustomer();
        ApiResponse<List<Customer>> response = new ApiResponse<>(
                customer,
                "Fetch all customers is successfully !!",
                true
        );
        return response;
    }

    // Get a customer by id
    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Integer id) {
        // Retrieve customer information from database or other data source
        Customer customer = customerService.getCustomerById(id);
            if (customer != null) {
                // Create ApiResponse object with payload, message, and success values
                ApiResponse<Customer> response = new ApiResponse<>(
                        customer,
                        "This customer was found !!",
                        true
                );
                // Return ResponseEntity with success ApiResponse
                return ResponseEntity.ok(response);
            }
        // Return ResponseEntity with 404 status code and no response body
        return ResponseEntity.notFound().build();
    }

    // Add new Customer
    @PostMapping("/add-new-customer")
    public ResponseEntity<?> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        // Retrieve customer information from database or other data source
        Customer addCustomer = customerService.addNewCustomer(customerRequest);
        // Create ApiResponse object with payload, message, and success values
        ApiResponse<Customer> response = new ApiResponse<>(
                addCustomer,
                "Customer was added successfully!!",
                true
        );
        return ResponseEntity.ok(response);
    }

    // Update Customer
    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest){
        // Retrieve customer information from database or other data source
        Customer customer = customerService.getCustomerById(id);
        if (customer != null) {
            customerService.updateCustomerById(id, customerRequest);
            Customer updatedCustomer = customerService.getCustomerById(id);
            // Create ApiResponse object with payload, message, and success values
            ApiResponse<Customer> response = new ApiResponse<>(
                    updatedCustomer,
                    "Customer was updated successfully!!",
                    true
            );
            return ResponseEntity.ok(response);
        }
        // Return ResponseEntity with 404 status code and no response body
        return ResponseEntity.notFound().build();
    }


    // Delete customer
    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable Integer id){
       Customer customer = customerService.deleteCustomerById(id);
           ApiResponse<Customer> response = new ApiResponse<>(
                   customer,
                   "Customer was deleted successfully!!",
                   true
           );
           return ResponseEntity.ok(response);
       }

    }


