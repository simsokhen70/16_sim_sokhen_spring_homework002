package com.example.mybatis.Controller;

import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Model.entity.Product;
import com.example.mybatis.Model.request.ProductRequest;
import com.example.mybatis.Model.response.ApiResponse;
import com.example.mybatis.Service.Product.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    // Get all product
    @GetMapping("/get-all-product")
    public ApiResponse getAllProduct() {
        // Retrieve customer information from database or other data source
        List<Product> products = productService.getAllProduct();
        ApiResponse<List<Product>> response = new ApiResponse<>(
                products,
                "Fetch all products is show to export database from datagriduccessfully !!",
                true
        );
        return response;
    }

    // Get a product by id
    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable("id") Integer id) {
        // Retrieve product information from database or other data source
        Product product = productService.getProductById(id);
        if (product != null) {
            // Create ApiResponse object with payload, message, and success values
            ApiResponse<Product> response = new ApiResponse<>(
                    product,
                    "This product was found !!",
                    true
            );
            // Return ResponseEntity with success ApiResponse
            return ResponseEntity.ok(response);
        }
        // Return ResponseEntity with 404 status code and no response body
        return ResponseEntity.notFound().build();
    }

    // Add new Product
    @PostMapping("/add-new-product")
    public ResponseEntity<?> addNewCustomer(@RequestBody ProductRequest productRequest){
        // Retrieve product information from database or other data source
        Product addProduct = productService.addNewProduct(productRequest);
        // Create ApiResponse object with payload, message, and success values
        ApiResponse<Product> response = new ApiResponse<>(
                addProduct,
                "Product was added successfully!!",
                true
        );
        return ResponseEntity.ok(response);
    }

    // Update Product
    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<?> updateProductById(@PathVariable Integer id, @RequestBody ProductRequest productRequest){
        // Retrieve product information from database or other data source
        Product product = productService.getProductById(id);
        if (product != null) {
            productService.updateProductById(id, productRequest);
            Product updatedProduct = productService.getProductById(id);

            // Create ApiResponse object with payload, message, and success values
            ApiResponse<Product> response = new ApiResponse<>(
                    updatedProduct,
                    "Customer was updated successfully!!",
                    true
            );
            return ResponseEntity.ok(response);
        }
        // Return ResponseEntity with 404 status code and no response body
        return ResponseEntity.notFound().build();
    }

    // Delete product
    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable Integer id) {
        Product product = productService.deleteProductById(id);
        ApiResponse<Product> response = new ApiResponse<>(
                product,
                "Product was deleted successfully!!",
                true
        );
        return ResponseEntity.ok(response);
    }
}
