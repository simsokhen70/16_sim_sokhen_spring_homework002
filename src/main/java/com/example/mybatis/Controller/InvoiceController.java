package com.example.mybatis.Controller;

import com.example.mybatis.Model.entity.Customer;
import com.example.mybatis.Model.entity.Invoice;
import com.example.mybatis.Model.entity.Product;
import com.example.mybatis.Model.request.CustomerRequest;
import com.example.mybatis.Model.request.InvoiceRequest;
import com.example.mybatis.Model.response.ApiResponse;
import com.example.mybatis.Service.Invoice.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
        public ApiResponse<List<Invoice>> getAllInvoice(){
        List<Invoice> invoices = invoiceService.getAllInvoice();
        ApiResponse<List<Invoice>> response = new ApiResponse<>(
                invoices,
                "Fetch all invoice is successfully !!",
                true
        );
        return response;
    }

    // Get invoice by id
    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable("id") Integer id) {
        // Retrieve invoice information from database or other data source
        Invoice invoice = invoiceService.getInvoiceById(id);
        if (invoice != null) {
            // Create ApiResponse object with payload, message, and success values
            ApiResponse<Invoice> response = new ApiResponse<>(
                    invoice,
                    "This invoice was found !!",
                    true
            );
            // Return ResponseEntity with success ApiResponse
            return ResponseEntity.ok(response);
        }
        // Return ResponseEntity with 404 status code and no response body
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/add-new-invoice")
    public ResponseEntity<?> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest) {
        Integer invoice = invoiceService.addNewInvoice(invoiceRequest);

        ApiResponse<Invoice> response = new ApiResponse<>(
                null,
                "Invoice was added successfully!",
                true
        );

        return ResponseEntity.ok(response);
    }

}
